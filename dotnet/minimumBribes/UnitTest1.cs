using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using NUnit.Framework;
using MinimumBribes;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase(2,1,5,3,4)]
        public void Test1(params int[] arr)
        {
            var t = Main.Test(arr);
            Assert.AreEqual(t, true);
        }
    }
}