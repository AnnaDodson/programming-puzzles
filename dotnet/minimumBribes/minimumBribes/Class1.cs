﻿using System;

namespace MinimumBribes
{
    public class Main
    {
        public static bool Test(int[] q)
        {
            // 1, 2, 3, 4, 5
            
            // 1, 2, 3, 5, 4
            // 1, 2, 5, 3, 4
            // 2, 1, 5, 3, 4

            var orig = new int[] {1, 2, 3, 4, 5};
            var result = new int[5];

            for (var i = 0; i < (q.Length - 1); i++)
            {
                if (q[i] > orig[i])
                {
                    var temp = orig[i];
                    orig[i] = orig[i+1];
                    orig[i + 1] = temp;
                }

            }
            
            for(var i = 0; i < q.Length ; i++){
                Console.WriteLine(q[i]);

            } 
            return true;
        }
    }
}
