﻿using System;
using System.Linq;

namespace repeated_string
{
    public class Main
    {
        public static long RepeatString(string letters, long repeats)
        {
            var howManyIn = repeats / letters.Length ;

            var leftOvers = repeats - (howManyIn * letters.Length);  

            var howManyLetters = letters.Count(x => x == 'a');

            var result = howManyLetters * howManyIn;

            for (var i = 0; i < leftOvers; i++)
            {
                if (letters[i] == 'a')
                {
                    result++;
                }
            }
            
            return result;
        }
    }
}
