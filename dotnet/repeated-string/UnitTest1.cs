using NUnit.Framework;
using repeated_string;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("aba", 10, 7)]
        [TestCase("a", 1000000000000, 1000000000000)]
        [TestCase("aab", 10, 7)]
        [TestCase("aabb", 10, 6)]
        [TestCase("bbaa", 10, 4)]
        [TestCase("aab", 882787, 588525)]
        public static void Test1(string input, long total, long expected)
        {
            var result = Main.RepeatString(input, total);
            
            Assert.AreEqual(expected, result, $"Expected {expected} but got {result}");
        }
    }
}