# Challenges

Repo for programming challenges from Hackerrank or other playground sites.

To run the dotnet tests, cd into the dotnet solution directory and run:

`
$ dotnet test
`
